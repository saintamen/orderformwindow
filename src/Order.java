
public class Order {

	private String name;
	private String surname;
	private String yearOfBirth;
	private Sex sex;
	private boolean isTaken;
	private String content;

	public Order(String name, String surname, String yearOfBirth, Sex sex, boolean isTaken, String content) {
		super();
		this.name = name;
		this.surname = surname;
		this.yearOfBirth = yearOfBirth;
		this.sex = sex;
		this.isTaken = isTaken;
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getYearOfBirth() {
		return yearOfBirth;
	}

	public void setYearOfBirth(String yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public boolean isTaken() {
		return isTaken;
	}

	public void setTaken(boolean isTaken) {
		this.isTaken = isTaken;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return name + " " + surname;
                //"Order [name=" + name + ", surname=" + surname + ", yearOfBirth=" + yearOfBirth + ", sex=" + sex
//				+ ", isTaken=" + isTaken + ", content=" + content + "]"
	}	
}
